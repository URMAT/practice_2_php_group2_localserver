<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 19.06.18
 * Time: 19:48
 */

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Curl\Curl;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class uLoginController extends Controller
{
    const ULOGIN_DATA = 'ulogin-data';

    /**
     * @Route("/register-case", name="app-register-case")
     */
    public function registerCaseAction()
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $user = new User();
        $user->setEmail($userData['email']);

        $form = $this->createForm(
            'App\Form\ULoginRegisterType', $user, [
                'action' => $this
                    ->get('router')
                    ->generate('app-register-case2')
            ]
        );
        //$user['network'] - соц. сеть, через которую авторизовался пользователь
        //$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
        //$user['first_name'] - имя пользователя
        //$user['last_name'] - фамилия пользователя
        $this->get('session')->set(self::ULOGIN_DATA, $s);

        return $this->render('registration/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => null
        ]);
    }

    /**
     * @Route("/register-case2", name="app-register-case2")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registerCase2Action(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $userData = json_decode(
            $this
                ->get('session')
                ->get(self::ULOGIN_DATA),
            true
        );

        $user = new User();

        $form = $this->createForm(
            'App\Form\ULoginRegisterType',
            $user
        );

        $form->handleRequest($request);

        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $user->setPassword("social------------------------".time());
                    $user->setSocialId($userData['network'], $userData['uid']);
                    $data = $user->__toArray();

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);

                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("main_page");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('registration/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/login_by_social", name="app_social_login")
     * @Method("Post")
     */
    public function socialSingUpAction(
            UserRepository $userRepository,
            UserHandler $userHandler
    )
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $user = $userRepository->getByuId($userData['uid']);
        if($user)
        {
            $userHandler->makeUserSession($user);
            return $this->redirectToRoute('main_page');
        }
        $error = 'Ты еще не зарегистрировал свою социальную сеть в нашей базе';
        return $this->render('error_page.html.twig', ['error' => $error]);
    }

    /**
     * @Route("/profile_page", name="app_profile_page")
     * @Method({"POST", "GET"})
     */
    public function profilePageAction(Request $request)
    {
        $currentUser = $this->getUser();
        return $this->render('profile_page.html.twig', ['user' => $currentUser]);
    }

    /**
     * @Route("/include_social")
     * @Method({"POST"})
     * @throws ApiException
     */
    public function includeSocialIdAction(
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $currentUser = $this->getUser();
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $currentUser->setSocialId($userData['network'], $userData['uid']);

        $test = $this->addToClientSocialNetworkAction($currentUser->getEmail(), $userData['network'], $userData['uid']);



        $manager->flush();

        return $this->redirectToRoute('app_profile_page');
    }


    public function addToClientSocialNetworkAction($email, $network, $uid)
    {
        $userVerification = new Curl();
        $userVerification->get('http://127.0.0.1:8001/add_to_client_social_network', [
            'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmdhbml6YXRpb25OYW1lIjoiXHUwNDIwXHUwNDNlXHUwNDMzXHUwNDMwIFx1MDQzOCBcdTA0MWFcdTA0M2VcdTA0M2ZcdTA0NGJcdTA0NDJcdTA0MzAiLCJvcmdhbml6YXRpb25TaXRlVVJMIjoiaHR0cDpcL1wvZHVyYWtvdi5uZXQiLCJpZCI6MX0.lqAZr0ljbPAhArybaH_CI2k--64UOqFEA7zRbkUllQo',
            'email_user' => $email,
            'network' => $network,
            'uid' => $uid
            ]
        );

        if ($userVerification->error) {
            return $userVerification->response = 'Error: ' . $userVerification->errorCode . ': ' . $userVerification->errorMessage . "\n";
        } else {
            return $userVerification->response;
        }
    }
}